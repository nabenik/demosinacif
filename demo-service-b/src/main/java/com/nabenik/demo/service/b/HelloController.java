package com.nabenik.demo.service.b;

import com.nabenik.demo.service.b.client.PillinadasClient;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 *
 */
@Path("/hello")
@Singleton
public class HelloController {
    
    @Inject
    @RestClient
    PillinadasClient pillinadasClient;

    String mensaje = "Hola mundo \n";
    
    @GET
    @Operation(description = "Operacion que combina la salida de servicio A"
            + " (pillinada) con servicio B")
    @APIResponse(responseCode = "200", description = "Texto plano con los datos "
            + "combinados")
    public String sayHello() {
        
        mensaje += "Las pillinadas del dia son \n";
        
        pillinadasClient.listAll().forEach(x -> {
            mensaje += x.getAuthor() + "-" +  x.getDescription() + " \n";
        });
        
        return mensaje;
    }
}
