package com.nabenik.demodi;

/**
 *
 * @author tuxtor
 */
public interface Vehiculo {
    
    public String avanzar();
    public String frenar();
    
}
