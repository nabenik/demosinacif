package com.nabenik.demodi;

/**
 *
 * @author tuxtor
 */
public class Principal {
    
    //Inyector - 3ro. Cohesion debil
    //Testing
    //Software polimorfico
    public static void main(String args[]) {
        
        //Separe la responsabilidad de creacion de dependencia
        Vehiculo v1 = new Automovil("Mazda", 2015L, "Rojo");
        Vehiculo v2 = new Moto();
        
        //Inyectando la dependencia
        Conductor pedro = new Conductor("Pedro Pan", v2);
        
        var resultado = pedro.conducir();
        
        System.out.println(resultado);
    }
    
}
