/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo5.cdi.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.interceptor.InterceptorBinding;

@Inherited
@InterceptorBinding //1- Objetivo
@Retention(RetentionPolicy.RUNTIME) //2- En donde vive
@Target({ElementType.METHOD, ElementType.TYPE}) //2- Quien la puede usar
public @interface Loggable {
    
}
