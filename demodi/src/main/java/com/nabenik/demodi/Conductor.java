package com.nabenik.demodi;

public class Conductor {
    
    //Agregacion
    //Dependencia - Para ejecutar su responsabilidad
    //necesito de este objeto
    private Vehiculo vehiculo;
    
    private String nombre;
    
    //Inyectar o proveer la dependencia
    public Conductor(String nombre, Vehiculo vehiculo) {
        this.nombre = nombre;
        //this.vehiculo = new Automovil("Honda", 2008L, "negro");
        this.vehiculo = vehiculo;
    }
    
    public String conducir() {
        return "Soy el conductor: " + this.nombre 
                + " " + this.vehiculo.avanzar();
    }
    
}
