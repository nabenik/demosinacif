package com.nabenik.demo5.cdi.service;

import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

//Caracteristicas
@Qualifier // 1- Objetivo
@Retention(RetentionPolicy.RUNTIME) //2- Permanece solo en comp, ejecucion, ambos
@Target({METHOD, FIELD, PARAMETER, TYPE})//3- Sobre que miembros de la clase funciona
//Nombre de la anotacion
public @interface INACIFInjectable {
    
}
