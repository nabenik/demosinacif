
package com.nabenik.demo.service.b.client;

import com.nabenik.demo.service.b.model.PillinadasDTO;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * Contrato, Microprofile va a generar el codigo por mi
 */
@Path("/pillinadas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface PillinadasClient {
    
    @GET
    public List<PillinadasDTO> listAll();
    
    @POST
    public Response addPillinada(PillinadasDTO pillinadasDTO);
    
}
