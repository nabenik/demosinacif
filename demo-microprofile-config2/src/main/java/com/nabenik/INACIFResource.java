/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik;

import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/inacif")
@RequestScoped
public class INACIFResource {
    
    @Inject
    @ConfigProperty(name = "JAVA_HOME", defaultValue = "Java's home")
    String javaHome;
            
    @GET
    @Path("/hello")
    public String doHello() throws UnknownHostException{
        var ip = InetAddress.getLocalHost().toString();
        return "Hola desde " + javaHome + " ejecutandose en " + ip;
    }
    
}
