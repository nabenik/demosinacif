package com.nabenik.demo.service.a.controller;

import com.nabenik.demo.service.a.model.PillinadasDTO;
import com.nabenik.demo.service.a.repository.PillinadasRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@ApplicationScoped
@Path("/pillinadas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PillinadasController {
    
    @Inject
    PillinadasRepository pillinadasRepository;
    
    @GET
    public List<PillinadasDTO> listAll(){
        return pillinadasRepository.getPillinadas();
    }
    
    @POST
    public Response create(PillinadasDTO pillinadasDTO) {
        pillinadasRepository.addPillinadas(pillinadasDTO);
        return Response.ok().build();
    }
    
}
