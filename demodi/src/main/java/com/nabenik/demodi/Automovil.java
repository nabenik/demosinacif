package com.nabenik.demodi;

/**
 *
 * @author tuxtor
 */
public class Automovil implements Vehiculo{
    
    private String marca;
    private Long modelo;
    private String color;
    
    public Automovil(){
        this.marca = "";
        this.modelo = 2020L;
        this.color = "";
    }

    public Automovil(String marca, Long modelo, String color) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Long getModelo() {
        return modelo;
    }

    public void setModelo(Long modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String avanzar() {
        return "Soy el carro " + this.color + " y estoy avanzando";
    }

    @Override
    public String frenar() {
        return "Soy el carro " + this.color + " y estoy frenando";
    }
    
    
    
    
}
