/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo5.cdi.service;

/**
 *
 * @author tuxtor
 */
public interface MessageProducer {
    public String createMessage(String name);
}
