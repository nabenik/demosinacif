/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo5.cdi.service;

import com.nabenik.demo5.cdi.util.Loggable;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

/**
 *
 * @author tuxtor
 */
@ApplicationScoped
@Alternative
public class FunnyMessageService implements MessageProducer{
    
    @Inject
    Logger logger;

    @Override
    @Loggable
    public String createMessage(String name) {
        logger.info("Antes de crear el mensaje");
        return "Holi " + name + " como estas?";
    }
    
}
