/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demodi;

public class Moto implements Vehiculo{

    @Override
    public String avanzar() {
        return "Soy una moto y avanzo entre carriles, run run";
    }

    @Override
    public String frenar() {
        return "Soy una moto y frene abruptamente porque se metio un ciclista";
    }
    
}
