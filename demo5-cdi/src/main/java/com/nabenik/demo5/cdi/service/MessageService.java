package com.nabenik.demo5.cdi.service;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author tuxtor
 */
@SessionScoped
public class MessageService implements Serializable, MessageProducer {
    
    private int contador;
    
    //Solo metodos
    @Override
    public String createMessage(String name){
        return "Hola " + name + " gusto en conocerte, mensaje " + contador++;
    }
    
}
