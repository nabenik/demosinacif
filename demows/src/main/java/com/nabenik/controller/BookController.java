package com.nabenik.controller;

import com.nabenik.application.BookService;
import com.nabenik.entities.Book;
import java.net.URI;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/books")
public class BookController {
    
    @Inject
    BookService bookService;
    
    @GET
    public List<Book> listAll(){
        return bookService.getAllBooks();
    }
    
    @POST
    public Response addBook(Book book){
        bookService.addBook(book);
        
        return Response.created(URI.create("/books" + book.getBookId())).build();
    }
    
}
