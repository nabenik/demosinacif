package com.nabenik.demo5.cdi.util;

import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@Loggable
@Priority(Interceptor.Priority.APPLICATION)
public class INACIFInterceptor {
    
    @Inject
    Logger logger;
    
    @AroundInvoke
    public Object generateLogBeforeInvoke(InvocationContext ic) throws Exception {
        try {
            logger.info("Interceptando el metodo");
            return ic.proceed();
        } finally {
            logger.info("Despues de proceder");
        }
        
    }
}
