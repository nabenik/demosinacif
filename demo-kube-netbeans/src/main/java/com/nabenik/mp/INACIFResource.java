package com.nabenik.mp;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
@Path("/inacif")
public class INACIFResource {
    
    @Inject
    @ConfigProperty(name = "inacif.value", defaultValue = "mensaje")
    String mensaje;
    
    @GET
    public String getInfo(){
        return "El mensaje es: " + mensaje;
        
    }   
}
