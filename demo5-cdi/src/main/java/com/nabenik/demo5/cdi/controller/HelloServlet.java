package com.nabenik.demo5.cdi.controller;

import com.nabenik.demo5.cdi.events.Persona;
import com.nabenik.demo5.cdi.service.INACIFInjectable;
import com.nabenik.demo5.cdi.service.MessageProducer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tuxtor
 */
@WebServlet(name = "HelloServlet", urlPatterns = {"/hello"})
public class HelloServlet extends HttpServlet{
    
    @Inject
    MessageProducer messageService;
    
    @Inject
    Event<Persona> personaEvent;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{
        
        Persona persona = new Persona();
        persona.setNombres("Victor Leonel");
        persona.setApellidos("Orozco Lopez");
        
        personaEvent.fire(persona);
        
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        out.println("<p>Hola amigos de INACIF </p> <br/>");
        out.println("<p>" + this.messageService.createMessage("Victor") + " </p>");
    }
    
}
