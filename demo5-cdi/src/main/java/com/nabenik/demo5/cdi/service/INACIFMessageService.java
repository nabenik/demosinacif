/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo5.cdi.service;

import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
@INACIFInjectable //Calificador de forma explicita y Type Safe
public class INACIFMessageService implements MessageProducer {
    
    private Logger logger;
    
    @Inject //.net, Angular
    public INACIFMessageService(Logger logger){
        this.logger = logger;
    }
    
    //Inicializacion
    @PostConstruct
    public void init(){
        logger.info("Inicializando objeto");
    }
    //Destruccion
    @PreDestroy
    public void destroy(){
        logger.warning("Adios desde INACIFMessageService");
    }
    
    @Override
    public String createMessage(String name) {
        logger.warning("Se esta ejecutando el metodo de INACIF");
        return "Por un INACIF, moderno, fiable y diligente";
    }
    
}
