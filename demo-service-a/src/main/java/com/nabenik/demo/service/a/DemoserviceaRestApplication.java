package com.nabenik.demo.service.a;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;

/**
 *
 */
@ApplicationPath("/data")
/*@OpenAPIDefinition(
    info = @Info(
            title = "Servicio de pillinadas",
            version = "1.0.0",
            contact = @Contact(
                    name = "Victor Orozco",
                    email = "vorozco@nabenik.com"
            )
    )
)*/
public class DemoserviceaRestApplication extends Application {
}
