package com.nabenik.demo.service.a.repository;

import com.nabenik.demo.service.a.model.PillinadasDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

@Dependent
public class PillinadasRepository {
    
    // "Base de datos"
    List<PillinadasDTO> pillinadasList;
    
    @PostConstruct
    public void init(){
        pillinadasList = new ArrayList<>();
        pillinadasList.add(
                new PillinadasDTO("Probar 10 minutos el microservicio que no es",
                        "Victor Orozco"));
    }
    
    public List<PillinadasDTO> getPillinadas(){
        return this.pillinadasList;
    }
    
    public void addPillinadas(PillinadasDTO pillinadasDTO){
        pillinadasList.add(pillinadasDTO);
    }
    
}
