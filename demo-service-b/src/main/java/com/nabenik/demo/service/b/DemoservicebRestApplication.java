package com.nabenik.demo.service.b;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;

/**
 *
 */
@OpenAPIDefinition(
    info = @Info(
            title = "Servicio de consumo de pillinadas",
            version = "1.0.0",
            contact = @Contact(
                    name = "Victor Orozco",
                    email = "vorozco@nabenik.com"
            )
    )
)
@ApplicationPath("/data")
@ApplicationScoped
public class DemoservicebRestApplication extends Application {
}
