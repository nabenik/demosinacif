
package com.nabenik.demo5.cdi.util;

import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author tuxtor 
 */
@ApplicationScoped
public class LogProducer {
    //Metodo "Fabrica"
    @Produces
    @Default
    public Logger produceLogger(InjectionPoint ip){
        return Logger.getLogger(ip.getBean().getBeanClass().toString());
        //return Logger.getLogger(LogProducer.class.getName());
    }
    
    //Metodo "Destructor"
    public void destroyLogger(@Disposes Logger logger){
        //Cierre
        logger.info("A punto de ser destruido");
    }
}
