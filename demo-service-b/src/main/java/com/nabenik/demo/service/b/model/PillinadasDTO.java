package com.nabenik.demo.service.b.model;


public class PillinadasDTO {
    private String description;
    private String author;
    
    public PillinadasDTO(){
        this.description = "";
        this.author = "";
    }

    public PillinadasDTO(String description, String author) {
        this.description = description;
        this.author = author;
    }
    
    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
    
    
}
