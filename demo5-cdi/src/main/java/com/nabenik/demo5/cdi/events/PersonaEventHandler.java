/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo5.cdi.events;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

/**
 *
 * @author tuxtor
 */
@ApplicationScoped
public class PersonaEventHandler implements Serializable {
    
    @Inject
    Logger logger;
    
    public void processPerson(@Observes Persona persona){
        logger.info("La persona que recibi fue " + persona.getNombres());
    }
    
}
